笔记jijiji

数据类型

int（整数）

精度10位，长度为4字节，数值范围为-231～231-1。

smallint（短整数）

精度为10位，长度为2字节，数值范围为-215～215–1。

char[(n)]

固定长度字符数据类型，其中n定义字符型数据的长度，n在1~8000之间，默认值为1。

varchar[(n)]

可变长度字符数据类型，其中n的规定与定长字符数据类型char[(n)]中n完全相同，与char[(n)]不同的是 varchar(n) 数据类型的存储空间随列值的字符数而变化。

text

最大长度为231-1(2,147,483,647)个字符，存储字节数与实际字符个数相同。

date

date类型可表示从公元元年1月1日到9999年12月31日期，表示形式与datetime数据类型的日期部分相同，只存储日期数据，不存储时间数据，存储长度为3个字节。

time

time数据类型只存储时间数据，表示格式为“hh:mm:ss[.nnnnnnn]”。

table

用于存储结果集的数据类型，结果集可以供后续处理。



创建数据库

create database 数据库名

on[primary]

删除数据库

drop database 数据库名

创建表

create table 表名

（

字段 1 数据类型 属性 约束，

字段2 数据类型 属性 约束，

...

） 

删除表

drop table 表名

约束

常见的约束：主键约束 唯一约束 检查约束 默认约束 外键约束

select语句

select <字段列表>from<表名>[where<条件表达式>]

insert语句

insert[into]<表名>[(列名)]values<值>

其中，into可以省略；表名是必须的，不能省略。



比较运算符

大小比较 包括>,>=,=,<,<=,<>,!>,!<

范围运算符

between   and

not between and

列表运算符

in(项1，项2)

not in(项1,项2)

逻辑运算符

not

and

or



对查询结果进行排序

order by    asc递增     desc递减 可以与top关键字一起使用、

聚合函数

sum()用于统计数值型字段的总合

avg()用于计算一个数值型字段的平均值

max()用于返回表达式中的最大值

min()用于返回表达式中的最小值

（1）多条件查询：

select *from 表名
where 条件表达式  and  条件表达式

模糊查询: like

select *from 表名
where 字段 like

消除重复行：distnct

select distinct 字段1，字段2 from 表名





1 交叉连接

select  字段名  from 表1cross join 表2 where

2 等值连接（比较运算符为: =）

==用法:1.聚合函数里，无列名要起别名,字段别名 2.在连接查询里面 表太多，太长，对表起别名==

3 自身连接（自身与自身连接）

select a.字段，b.字段
from 表名1 别名a，表名1 别名b
where a.字段 = b.字段

4 左/右连接：left/right join

select 表1别名.列1,表2别名.列2 from 表1 别名
left join 表2 别名 on 表1别名.列=表2别名.列

select 表1别名.列1,表2别名.列2 from 表1 别名
right join 表2 别名 on 表1别名.列=表2别名.列



5 内连接（与等值连接类似）

select 表1别名.列1,表2别名.列2 from 表1 别名
inner join 表2 别名 on 表1别名.列=表2别名.列
inner join 表3 别名 on 表2别名.列=表3别名.列
where 条件表达式
grop by
order by